﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Timers;


namespace DocumentsReceiver
{
    public class DocumentsReceiver: IDisposable
    {
        public event Action DocumentsReady;
        public event ElapsedEventHandler TimedOut;

        private readonly Timer _timer;
        private readonly FileSystemWatcher _watcher;
        private readonly List<string> _list;
        private readonly string[] _validNames;
        private readonly ILogger _logger;

        public DocumentsReceiver(ILogger logger, params string[] validNames)
        {
            _timer = new Timer();
            _watcher = new FileSystemWatcher();
            _list = new List<string>();
            _validNames = validNames;
            _logger = logger;
            Subscribe();
        }

        private void Subscribe()
        {
            _timer.Elapsed += _timer_Elapsed;
            
            TimedOut += DocumentsReceiver_TimedOut;
            DocumentsReady += DocumentsReceiver_DocumentsReady;
            
            _watcher.Created += _watcher_Created;
            _watcher.Renamed += _watcher_Renamed;
            _watcher.Deleted += _watcher_Deleted;
        }

        public void Start(string targetDirectory, double waitingInterval)
        {
            if (waitingInterval < 0)
            {
                _logger.Log("Время ожидания не может быть меньше 0");
                return;
            }

            if (string.IsNullOrEmpty(targetDirectory) || !Directory.Exists(targetDirectory))
            {
                _logger.Log("Путь задан некорректно");
                return;
            }

            _logger.Log("Старт приложения...");

            _timer.Interval = waitingInterval;
            _timer.Start();
            _watcher.Path = targetDirectory;
            _watcher.EnableRaisingEvents = true;
        }

        private void Stop()
        {
            _logger.Log("Остановка приложения...");

            _timer.Stop();
            _watcher.EnableRaisingEvents = false;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut?.Invoke(sender, e);
        }

        private void DocumentsReceiver_TimedOut(object sender, ElapsedEventArgs e)
        {
            _logger.Log("Время истекло!");
            Stop();
        }

        private void DocumentsReceiver_DocumentsReady()
        {
            if (_list.Count == 3)
            {
                _logger.Log("Все документы приняты!");
                Stop();
            }
        }

        private void _watcher_Renamed(object sender, RenamedEventArgs e)
        {
            if (_validNames.Contains(e.OldName))
            {
                _logger.Log($"Файл {e.OldName} переименован!");
                _list.Remove(e.OldName);
            }
            else
            {
                _watcher_Created(sender, e);
            }
        }

        private void _watcher_Created(object sender, FileSystemEventArgs e)
        {
            if (_validNames.Contains(e.Name))
            {
                _logger.Log($"Файл {e.Name} добавлен!");
                _list.Add(e.Name);
                DocumentsReady?.Invoke();
            }
        }

        private void _watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            _logger.Log($"Файл {e.Name} удален!");
            _list.Remove(e.Name);
        }

        public void Dispose()
        {
            _timer.Elapsed -= _timer_Elapsed;
            TimedOut -= DocumentsReceiver_TimedOut;
            _watcher.Created -= _watcher_Created;
            _watcher.Renamed -= _watcher_Renamed;
            _watcher.Deleted -= _watcher_Deleted;
            DocumentsReady -= DocumentsReceiver_DocumentsReady;

            _timer.Close();
            _watcher.Dispose();
        }
    }
}
