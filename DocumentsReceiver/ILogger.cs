﻿namespace DocumentsReceiver
{
    public interface ILogger
    {
        void Log(string text);
    }
}
