﻿using System;

namespace EventLesson
{
    class Program
    {
        const string PATH = @"C:\1";
        const double INTERVAL = 50_000;

        static void Main(string[] args)
        {
            using DocumentsReceiver.DocumentsReceiver documentsReceiver = 
                new(new Logger(), "Паспорт.jpg","Заявление.txt","Фото.jpg");
            documentsReceiver.Start(PATH, INTERVAL);

            Console.ReadLine();
        }
    }
}
