﻿using System;
using DocumentsReceiver;

namespace EventLesson
{
    public class Logger : ILogger
    {
        public void Log(string text)
        {
            Console.WriteLine(text);
        }
    }
}
